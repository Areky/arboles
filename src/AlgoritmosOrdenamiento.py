#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor
__author__ = "documentodigital"
__date__ = "$Aug 17, 2018 6:56:22 PM$"

def SelectionSort(lista):
    for i in range(len(lista)-1): #n
        positionMin=i
        for j in range(i+1, len(lista)): #5n
            if lista[j]<lista[positionMin]:
                positionMin = j
        temp = lista[i]
        lista[i] = lista[positionMin]
        lista[positionMin] = temp
        
#Ordenamiento por insercion
def InsertionSort(cos): 
    for ind in range(1,len(cos)): #2n
        valor = cos[ind]
        i = ind 
        while i>0 and cos[i-1]>valor: #2n
            cos[i]=cos[i-1]
            i=i-1
        cos[i] =valor #+1
    return

#Ordenamiento por burbuja
def BubbleSort(colec):
    
     for pasar in range(len(colec)-1,0,-1): #n
        for i in range(pasar):  #4n 
            if colec[i]>colec[i+1]:
                temp = colec[i]
                colec[i] = colec[i+1]
                colec[i+1] = temp
                
"""def BubbleOptim(cosas):
    Actu = True 
    n=len(cosas)
    while(Actu==True and n>1): #n
        Actu = False
        for i in range(len(cosas)-1): #6n
            if cosas[i]>cosas[i+1]:
                temp = cosas[i]
                cosas[i] = cosas[i+1]
                cosas[i+1] = temp
                Actu = True
        n-=1
    return cosas #+1"""

#Ordenamiento por mezcla
def MergeSort(cositas):
    if len(cositas) < 2:
        return cositas
    mitad = len(cositas)/2
    izq = MergeSort(cositas[:mitad])
    der = MergeSort(cositas[mitad:])
    
    return merge(izq,der)
    
def merge(lista1,lista2):
    i,j = 0,0
    resultado = []
    
    while(i < len(lista1) and j < len(lista2)):
        if (lista1[i] < lista2[j]):
            resultado.append(lista1[i])
            i += 1
        else:
            resultado.append(lista2[j])
            j += 1
    resultado += lista1[i:]
    resultado += lista2[j:]
    
    return resultado

#Ordenamiento veloz
def QuickSort(conju):
    
    if len(conju) < 2:
        return conju
    
    menores, medio, mayores = particion(conju)
    return QuickSort(menores)+medio+QuickSort(mayores)

def particion(conju):
    
    pivote = conju[0]
    menores = []
    mayores = []
    
    for x in xrange(1, len(conju)):
        if conju[x] < pivote:
            menores.append(conju[x])
        else:
            mayores.append(conju[x])
            
    return menores, [pivote], mayores