# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "Noe"
__date__ = "$21-sep-2018 0:47:05$"

from ArbolBinarioTarea import AB

''' Clase arbol, genera la estructura de un arbol binario, asi como sus funciones.'''
class ABB(AB):

    ''' Consructor de arboles'''
    def __init__(self, dato):
        super(ABB, self).__init__(dato)

    ''' Busca un dato en el arbol y devuelve el nodo'''
    def busqueda(self, nodo, dato):
        # si la raiz es None o el dato esta contenido en el nodo,
        # se devuelve el nodo.
        if nodo is None or nodo.dato == dato:
            return nodo
        # si el dato es mayo entonces se busca del lado derecho
        if nodo.dato < dato:
            return self.busqueda(nodo.der, dato)
        # si no, se busca en el lado izquierdo
        else:
            return self.busqueda(nodo.izq, dato)

    ''' Inserta un elemento en el arbol'''
    def insertar_nodo(self, nodo, dato):
        # si el nodo es vacio ahi se crea el nuevo nodo
        if nodo is None:
            nuevo_nodo = self.crea_nodo(dato)
            self.ultimo_agregado = nuevo_nodo
            return nuevo_nodo
        # si el dato es menor que su padre, se inserta en el lado izquierdo
        if dato < nodo.dato:
            nuevo_nodo = self.insertar_nodo(nodo.izq, dato)
            nodo.izq = nuevo_nodo
            nuevo_nodo.padre = nodo
        # de no ser asi se inserta del lado derecho
        else:
            nuevo_nodo = self.insertar_nodo(nodo.der, dato)
            nodo.der = nuevo_nodo
            nuevo_nodo.padre = nodo
        #nodo guarda toda la ruta de donde sera insertado el dato
        #hasta caer en el caso base, es por eso que se devuelve    
        return nodo
    
    ''' Inserta un nodo en el arbol, a partir de la raiz'''
    def insertar(self, dato):
        #Se inserta el dato desde la raiz
        self.insertar_nodo(self.raiz, dato)
        
    ''' Inserta un nodo en el arbol, a partir de la raiz'''
    def minimo_ensubarbol(self, nodo):
        if nodo.izq is None or nodo is None:
            return nodo
        return self.minimo_ensubarbol(nodo.izq)

def main():
    #print "Aqui codigo de la tarea"
    abb = ABB(10)
    abb.insertar(5)
    abb.insertar(15)
    abb.insertar(7)
    abb.insertar(25)
    abb.insertar(17)
    #print abb.busqueda(abb.raiz, 55)
    #print abb.busqueda(abb.raiz, 17)
    print abb.recorrido_enorden(abb.raiz)
    #print abb.minimo_ensubarbol(abb.raiz.izq)

if __name__ == "__main__":
    main()