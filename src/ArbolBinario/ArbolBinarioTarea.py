__author__ = "Noe"
__date__ = "$21-sep-2018 0:17:57$"

class Nodo(object):
    ''' Clase Nodo necesaria para generar arboles binarios'''
    def __init__(self, value):
        self.izq = None
        self.dato = value
        self.der = None
        self.padre = None
        
    def esIzq(self):
        return self.padre.izq is self
    
    def esDer(self):
        return self.padre.der is self
    
    def __str__(self):
        if self.dato is None:
            pass
        else:
            return '{}'.format(self.dato)
        
class AB(object):

    ''' Consructor de arboles'''
    def __init__(self, dato):
        self.raiz = self.crea_nodo(dato)
        self.ultimo_agregado = self.raiz

    ''' Crea un nodo'''
    def crea_nodo(self, dato):
        return Nodo(dato)

    ''' Devuelve el ultimo nodo agregado'''
    def ultimo(self):
        return self.ultimo_agregado

    ''' Devuelve el ultimo nodo agregado'''
    def elimina(self):
        self.raiz = None
        self.ultimo_agregado = None

    ''' Devuelve el nivel del nodo que se le pasa como parametro.
        La raiz se ubica en el nivel cero'''
    def nivel(self, nodo):
        if nodo is self.raiz:
            return 0
        else:
            return 1 + self.nivel(nodo.padre)

    ''' Devuelve la altura a partir del nodo que se le pasa como parametro,
        si el arbol es vacio la altura es cero, si no se le suma 1 al maximo
        de las alturas de sus hijos'''
    def altura(self, nodo):
        if nodo is None:
            return -1
        else:
            return 1 + max(self.altura(nodo.izq), self.altura(nodo.der))
        
        
    ''' Inserta un nodo en el arbol, a partir de la raiz'''
    def insertar(self, dato):
        nuevo = self.crea_nodo(dato)
        if self.raiz is None:
            self.raiz = nuevo
        else:
            temp = self.raiz
            while(temp.izq is not None):
                temp = temp.izq
            temp.izq = nuevo
            nuevo.padre = temp
        self.ultimo_agregado = nuevo
        
    ''' Recorre el arbol enorden e imprime cada uno de sus nodos'''
    def recorrido_enorden(self, raiz):
        if raiz is not None:
            self.recorrido_enorden(raiz.izq)
            print raiz.dato
            self.recorrido_enorden(raiz.der)

    ''' Recorre el arbol preorden e imprime cada uno de sus nodos'''
    def recorrido_preorden(self, raiz):
        if raiz is not None:
            print raiz.dato
            self.recorrido_preorden(raiz.izq)
            self.recorrido_preorden(raiz.der)

    ''' Recorre el arbol postorden e imprime cada uno de sus nodos'''
    def recorrido_postorden(self, raiz):
        if raiz is not None:
            self.recorrido_postorden(raiz.izq)
            self.recorrido_postorden(raiz.der)
            print raiz.dato
        
def main():
    
    #Se crea un arbol binario con el valor 10 en la raiz
    ab = AB(10)
    #Insertamos multiples valores
    ab.insertar(11)
    ab.insertar(5)
    ab.insertar(16)
    ab.insertar(17)
    ab.insertar(9)
    ab.insertar(20)
    #Probamos el metodo que calcula la altura del arbol
    print 'Altura desde la raiz'
    print ab.altura(ab.raiz)
   #Imprimimos el vivel de la raiz
    print 'Nivel de la raiz'
    print ab.nivel(ab.raiz)
    #Imprimimos el recorrrido en postorden
    print 'Recorrido en postorden'
    print ab.recorrido_postorden(ab.raiz)
    
if __name__ == "__main__":
    main()