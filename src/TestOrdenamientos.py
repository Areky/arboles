#! /usr/bin/python
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
__author__ = "mike"
__date__ = "$Aug 29, 2018 6:49:34 PM$"

from time import time
from random import randint 
#Import para poder usar los algoritmos definidos
import AlgoritmosOrdenamiento as ao

def generadorRandom(n):
    a=[]
    for i in range(n):
         a.append(randint(1,10000))
    return a

lista = generadorRandom(10000)
cos   = generadorRandom(10000)
colec = generadorRandom(10000)
conju = generadorRandom(10000)
cositas = generadorRandom(10000)
if __name__ == "__main__":
    #ejemplo de como usar los modulos importados
    
    start1 = time()
    ao.SelectionSort(lista)
    print lista
    end1 = time()-start1
    print("Tiempo de SelectionSort es:" +str(end1)+"segundos")
    
    start2 = time() 
    ao.BubbleSort(colec)
    print colec
    end2 = time()-start2
    print("Tiempo de BubbleSort es:" +str(end2)+"segundos")
    
    start4 = time()
    print ao.QuickSort(conju)
    end4 = time()-start4
    print("Tiempo de QuickSort es:" +str(end4)+"segundos")
    
    start5 = time()
    ao.InsertionSort(cos)
    print cos
    end5 = time()-start5
    print("Tiempo de InsertionSort es:" +str(end5)+"segundos")
    
    start6 = time()
    print ao.MergeSort(cositas)
    end6 = time()-start6
    print("Tiempo de MergeSort es:" +str(end6)+"segundos")
    
    
