#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "mike"
__date__ = "$Aug 15, 2018 5:38:29 PM$"

def busquedaLineal(coleccion, dato):
    for i in range(len(coleccion)):
        if coleccion[i] == dato:
            return i
    return -1

if __name__ == "__main__":
    col = [4,7,2,7,9,76,44,104,3,2,43,5,4634,3,45,34,3,54,78]
    print busquedaLineal(col, 500)

