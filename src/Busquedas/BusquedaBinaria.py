#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "mike"
__date__ = "$Sep 3, 2018 6:51:28 PM$"

def busquedaBinariaIngenua(lista, valor):
    if len(lista) == 1:
        return False
    mitad = len(lista)//2
    if lista[mitad] == valor:
        return mitad
    if lista[mitad] < valor:
        return busquedaBinariaIngenua(lista[mitad:], valor)
    else:
        return busquedaBinariaIngenua(lista[:mitad], valor)
    
def busquedaBinaria(lista, valor, izq, der):
    if der < izq:
        return False
    mitad = izq + ((der-izq)//2)
    if lista[mitad] == valor:
        return mitad
    if lista[mitad] < valor:
        return busquedaBinaria(lista, valor, mitad, der)
    else:
        return busquedaBinaria(lista, valor, izq, mitad)
    
def busquedaBinariaIterativa(lista, dato):
    prim = 0
    ulti = len(lista)-1
    
    while prim<=ulti :
        mitad = (prim + ulti)//2
        if lista[mitad] == dato:
            return mitad
        
        else:
            if dato<lista[mitad]:
                ulti = mitad-1
            else:
                prim = mitad+1
        
    return 
    
                
            
def main():
    l = [0, 20, 25, 45, 55, 65, 100, 200,300,400,555,666,777,888,999]
    
   #print busquedaBinariaIngenua(l, 56)
    
    print busquedaBinaria(l, 25, 0, len(l))
    
    print busquedaBinariaIterativa(l, 777)

if __name__ == "__main__":
    main()
