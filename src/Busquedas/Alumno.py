from Ordenamientos import AlgoritmosOrdenamiento as ao
from Busquedas import BusquedaBinaria as bb

'''
Esta clase muestra la forma en la que se puede emplear programacion orientada
a objetos (POO) en python. La clase alumno es una representacion basica de los
datos que nos interesa conocer de algun alumno.
'''
class Alumno:
    
    #Constructor de alumnos
    def __init__(self, numero_cuenta, nombre, edad):
        self.numero_cuenta = numero_cuenta
        self.nombre = nombre
        self.edad = edad
     
    #Print de alumnos    
    def __repr__(self):
        return "Numero de Cuenta: {0}, Nombre: {1}, Edad: {2} ".format(
                self.numero_cuenta,self.nombre,self.edad)
    
    #Sobrecarga del operador menor o igual "<"
    def __le__(self, alumno):
        if self.numero_cuenta <= alumno.numero_cuenta:
            return True
        else:
            return False
        
    #sobrecarga del operador mayor o igual ">"    
    def __ge__(self, alumno):
        if self.numero_cuenta >= alumno.numero_cuenta:
            return True
        else:
            return false
    
    #Sobrecarga del operador de equivalencia "=="
    def __eq__(self, alumno):
        if self.numero_cuenta == alumno.numero_cuenta:
            return True
        else:
            return False
        
def main():
    #Se crean varios alumnos
    alumno1 = Alumno(543453, 'Levi', 18)
    alumno2 = Alumno(89789, 'Eren', 14)
    alumno3 = Alumno(786786, 'Mikasa', 14)
    alumno4 = Alumno(98707, 'Armin', 13)
    
    #Dentro de la lista de alumnos se almacenan a los alumnos creados
    alumnos = [alumno1, alumno2, alumno3, alumno4]
    
    #Se ordena la lista de alumnos (IN-SITU)
    ao.SelectionSort(alumnos)
    
    #La lista de alumnos ya cambio de orden
    print alumnos
    
    #Buscamos en la lista de alumnos, al alumno1, desde el inicio hasta el final
    print bb.busquedaBinaria(alumnos, alumno1, 0, len(alumnos))
    
main()
    
